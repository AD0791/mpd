# MPD

This repository contains the spreadsheets and the files codes of the model: **MPD**:  
- This model will be use to simulate and predict the behavior of the Haitian economy.  
- A powerful tool for better decision making and better Planning for the Government.

> status: It's currently under construction.

# Language
> R

## IDE 

> Jupyter Notebook  
> Rstudio::Rmarkdown
